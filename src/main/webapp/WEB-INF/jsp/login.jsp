<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>YP OAuth Login</title>
</head>
<body>
<h2 class="page-header">YP OAuth Login</h2>

<div class="row">
    <div class="col-md-6">

        <div class="panel panel-default">
            <div class="panel-body">

                <form action="${pageContext.request.contextPath}/signin" method="post" class="form-horizontal">
                    <tags:csrf/>
                    <div class="form-group">
                        <label for="username" class="col-sm-3 control-label">用户名</label>

                        <div class="col-sm-9">
                            <input type="text" id="username" name="oidc_user" value="" placeholder="Type username"
                                   required="required" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="col-sm-3 control-label">密码</label>

                        <div class="col-sm-9">
                            <input type="password" name="oidcPwd" id="password" value="" placeholder="Type password"
                                   required="required" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="col-sm-3 control-label"></label>

                        <div class="col-sm-9">
                            <input type="submit" value="Login" class="btn btn-primary"/>
                            <%--Login error--%>
                            <c:if test="${param.error eq '2'}"><span
                                    class="label label-danger">拒绝访问 !!!</span></c:if>
                            <c:if test="${param.error eq '1'}"><span
                                    class="label label-danger">认证失败</span></c:if>
                        </div>
                    </div>

                </form>
            </div>
        </div>

    </div>
    <div class="col-md-6">
        <p>你可以使用以下几个初始的账号进行登录:</p>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>用户名</th>
                <th>密码</th>
                <th>备注</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>admin</td>
                <td>admin</td>
                <td>所有权限，允许访问[Mobile]和[Unity]资源，管理用户</td>
            </tr>
            <tr>
                <td>unity</td>
                <td>unity</td>
                <td>只允许访问[Unity]资源，支持grant_type:
                    <em>authorization_code,refresh_token,implicit</em></td>
            </tr>
            <tr>
                <td>mobile</td>
                <td>mobile</td>
                <td>只允许访问[Mobile]资源，支持grant_type: <em>password,refresh_token</em></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>


</body>
</html>