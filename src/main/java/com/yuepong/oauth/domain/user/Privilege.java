package com.yuepong.oauth.domain.user;

/**
 * @author Shengzhao Li
 */
public enum Privilege {

    USER,          //Default privilege

    ADMIN,     //admin
    UNITY,
    MOBILE
}