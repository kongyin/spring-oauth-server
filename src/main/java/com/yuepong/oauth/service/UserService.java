package com.yuepong.oauth.service;

import com.yuepong.oauth.service.dto.UserFormDto;
import com.yuepong.oauth.service.dto.UserJsonDto;
import com.yuepong.oauth.service.dto.UserOverviewDto;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author Shengzhao Li
 */
public interface UserService extends UserDetailsService {

    UserJsonDto loadCurrentUserJsonDto();

    UserOverviewDto loadUserOverviewDto(UserOverviewDto overviewDto);

    boolean isExistedUsername(String username);

    String saveUser(UserFormDto formDto);
}