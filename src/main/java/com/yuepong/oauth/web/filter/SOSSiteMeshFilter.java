package com.yuepong.oauth.web.filter;

import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;

/**
 *网络过滤
 */
public class SOSSiteMeshFilter extends ConfigurableSiteMeshFilter {


    public SOSSiteMeshFilter() {
    }


    @Override
    protected void applyCustomConfiguration(SiteMeshFilterBuilder builder) {

        builder.addDecoratorPath("/*", "/WEB-INF/jsp/decorators/main.jsp")

                .addExcludedPath("/static/**");


    }
}
